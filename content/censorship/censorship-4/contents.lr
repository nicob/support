_model: question 
---
title: I can’t connect to Tor Browser, is my network censored?
---
seo_slug: cannot-connect-to-tor-browser-network-censored
---
key: 11
---
description: 
You might be on a network that is blocking the Tor network, and so you should try using bridges.
Some bridges are built in to Tor Browser and requires only a few steps to enable it.
To use a pluggable transport, click "Configure Connection" when starting Tor Browser for the first time.
Under the "Bridges" section, locate the option "Choose from one of Tor Browser's built-in bridges" and click on "Select a Built-In Bridge" option.
From the menu, select whichever [pluggable transport](https://tb-manual.torproject.org/circumvention/) you'd like to use.

Once you've selected the pluggable transport, scroll up and click "Connect" to save your settings.

Or, if you have Tor Browser running, click on "Settings" in the hamburger menu (≡) and then on "Connection" in the sidebar.
Under the "Bridges" section, locate the option "Choose from one of Tor Browser's built-in bridges" and click on "Select a Built-In Bridge" option.
Choose whichever pluggable transport you'd like to use from the menu. Your settings will automatically be saved once you close the tab.

If you need other bridges, you can get them at our [Bridges website](https://bridges.torproject.org/).
For more information about bridges, see the [Tor Browser manual](https://tb-manual.torproject.org/bridges).
